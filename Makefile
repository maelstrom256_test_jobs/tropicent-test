#Makefile for Tropicent-test by khajiit

Unit=pyws.service
NginxConf=pyws.conf
AppDir=pyws
TargetDir=/opt/
NgSA=/etc/nginx/sites-available/
NgSE=/etc/nginx/sites-enabled/
SystemdDir=/lib/systemd/system/

#Silencing is_root checking
.SILENT: is_root

install.all: install wait5 test

install: install.app install.systemd install.nginx

is_root:
	if [ `id -u` -ne 0 ]; then echo '\033[0;31mYou must be root\033[0m'; exit 1; fi

install.app: is_root
	cp -r app/$(AppDir) $(TargetDir)

install.systemd: is_root
	cp systemd/$(Unit) $(SystemdDir)
	systemctl daemon-reload
	systemctl enable $(Unit)
	systemctl start $(Unit)

install.nginx: is_root
	cp nginx/$(NginxConf) $(NgSA)
	@echo 'Config stub added to sites-available. You may need manually set it up.'
	ln -s $(NgSA)$(NginxConf) $(NgSE)
	systemctl reload nginx

purge: is_root
	#purge nginx
	find $(NgSE) -name "$(NginxConf)" -delete
	find $(NgSA) -name "$(NginxConf)" -delete
	systemctl reload nginx
	#purge systemd
	systemctl stop $(Unit)
	systemctl disable $(Unit)
	find $(SystemdDir) -name "$(Unit)" -delete
	systemctl daemon-reload
	systemctl reset-failed
	#purge app
	rm -rf $(TargetDir)$(AppDir)

wait5:
	@echo Wait 5 seconds
	@ping localhost -c 5 > /dev/null
	@echo ...done

test:
	@echo Testing app webserver
	curl "localhost:8008"
	@echo Testing nginx http
	curl localhost
	@echo Testing https
	curl -k -L localhost
