# pyws Dockerfile for Tropicent-test by khajiit

# Pull base image.
FROM dockerfile/ubuntu

# Install git and make, clone and install project files
RUN \
  apt update && \
  apt install -y git make && \
  mkdir -p /root/projects && \
  git clone https://gitlab.com/maelstrom256/tropicent-test.git && \
  make install.app

# There you may define mountable directories.
#VOLUME []

# Define working directory,if needed.
#WORKDIR /opt/pyws

# Define default command.
CMD ["/opt/pyws/pyws"]

# Expose ports.
EXPOSE 80
